#!/bin/bash

sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
sudo apt-get update
sudo apt install wget git python3 python3-venv build-essential net-tools awscli nload zip net-tools -y

# install CUDA (from https://developer.nvidia.com/cuda-downloads)
wget -nv https://developer.download.nvidia.com/compute/cuda/12.0.0/local_installers/cuda_12.0.0_525.60.13_linux.run
sudo sh cuda_12.0.0_525.60.13_linux.run --silent

# install git-lfs
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
sudo -u ubuntu git lfs install --skip-smudge
git lfs install


# download AUTOMATIC1111/stable-diffusion-webui
cd /home/ubuntu
git clone https://github.com/AUTOMATIC1111/stable-diffusion-webui.git


# download the SD model v2.1 and move it to the SD model directory
sudo -u ubuntu git clone --depth 1 https://huggingface.co/stabilityai/stable-diffusion-2-1-base
cd stable-diffusion-2-1-base/
sudo -u ubuntu git lfs pull --include "v2-1_512-ema-pruned.ckpt"
sudo -u ubuntu git lfs install --force
cd ..
mv stable-diffusion-2-1-base/v2-1_512-ema-pruned.ckpt stable-diffusion-webui/models/Stable-diffusion/
rm -rf stable-diffusion-2-1-base/

# download the corresponding config file and move it also to the model directory (make sure the name matches the model name)
wget -nv https://raw.githubusercontent.com/Stability-AI/stablediffusion/main/configs/stable-diffusion/v2-inference.yaml
mv v2-inference.yaml stable-diffusion-webui/models/Stable-diffusion/v2-1_512-ema-pruned.yaml

# download controlnet
git clone https://github.com/Mikubill/sd-webui-controlnet.git stable-diffusion-webui/extensions/sd-webui-controlnet
# download controlnet models
git clone --depth=1 https://huggingface.co/lllyasviel/ControlNet stable-diffusion-webui/models/ControlNet

# download additional models
echo Downloading SD Models
cat stable-diffusion-server/models.txt | xargs -n 1 -P 5 wget -nv -P stable-diffusion-webui/models/Stable-diffusion/ --content-disposition 
echo Downloading VAE Models
cat stable-diffusion-server/vae.txt | xargs -n 1 -P 5 wget -nv -P stable-diffusion-webui/models/VAE/ --content-disposition 
echo Downloading Lora Models
cat stable-diffusion-server/lora.txt | xargs -n 1 -P 5 wget -nv -P stable-diffusion-webui/models/Lora/ --content-disposition 

# set permissions to user ubuntu
chown -R ubuntu stable-diffusion-webui
chown -R ubuntu stable-diffusion-server

# create stable.service for webui
cp stable-diffusion-server/stable.service /etc/systemd/system/stable.service

# create shutdown_sync.service
cp stable-diffusion-server/shutdown_sync.service /etc/systemd/system/shutdown_sync.service

systemctl daemon-reload


# enable and start shutdown_sync.service
systemctl enable shutdown_sync.service
systemctl start shutdown_sync.service


# enable and start stable.service
systemctl enable stable.service
systemctl start stable.service

# We need this somewhy, don't remember...
#stable-diffusion-webui/venv/bin/python -m pip install --upgrade fastapi==0.90.1 opencv-python
