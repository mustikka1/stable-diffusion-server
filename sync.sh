#!/bin/bash
S3_BUCKET_NAME=$(< /home/ubuntu/s3_bucket_name.txt)
if [ -n "$S3_BUCKET_NAME" ]; then
  aws s3 sync /home/ubuntu/stable-diffusion-webui/outputs s3://$S3_BUCKET_NAME/outputs
fi